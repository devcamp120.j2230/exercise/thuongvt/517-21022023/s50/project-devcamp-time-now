import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import Time from "./component/time";

function App() {
  return (
    <div className="container text-center mt-5">
      <div className="row justify-content-center">
        <div className="col-6">
        <Time></Time>
        </div>
      </div>
    </div>
  );
}

export default App;
