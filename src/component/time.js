import { Component } from "react";

class Time extends Component{
 
    constructor (props){
        super(props);
        
        let today  = new Date().toLocaleString()

        this.state = ({
            currentTime: today,
            second: new Date().getSeconds(),
            btnColor: "black"
        })
    }

    buttonClickChangeColorHandler = ()=>{
        if (this.state.second % 2 === 0) {
            this.setState({
                btnColor: "red"
            })
        } else {
            this.setState({
                btnColor: "blue"
            })
        }

    }

    render(){
        return(
            <>
            <div className="form-control mt-3">
                <h4 style={{color:this.state.btnColor}}>Hello, world!</h4>
            </div>
            <div className="form-control mt-3" style={{backgroundColor:"Highlight"}}>
                <p style={{color:"white"}}>
                    Time is: {this.state.currentTime}
                </p>
            </div>
            <div className="row mt-3">
                <button className="btn btn-success" onClick={this.buttonClickChangeColorHandler}>Stop</button>
            </div>
            </>
        )
    }
}
export default Time